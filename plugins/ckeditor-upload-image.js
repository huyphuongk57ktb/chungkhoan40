import apiUpload from '/apis/common'

export default class UploadAdapter {
  constructor(loader) {
    // The file loader instance to use during the upload.
    this.loader = loader;
  }

  async upload() {
    const url = await this.sendImage();

    return new Promise((resolve, reject) => {
      resolve({
        default: url
      });
    })
  }

  async sendImage() {
    let loader = await this.loader.file;

    const data = new FormData();
    data.append('files', loader);

    const image = await apiUpload.uploadImage(data);

    return image.url;
  }

  // Aborts the upload process.
  abort() {
    if (this.xhr) {
      this.xhr.abort();
    }
  }
}
