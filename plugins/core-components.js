import Vue from 'vue'

import CustomLoading from "../components/common/Spinner";

Vue.component('Loading', CustomLoading);
