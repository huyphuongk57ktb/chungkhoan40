import axios from 'axios';

export default {
  getListCategory: (data) => {
    const url = `${process.env.serverApi}/v1.0/category`;
    return new Promise((resolve, reject) => {
      axios.get(url, {params: data, withCredentials: true, xsrfCookieName: false,}, {
        headers: {
          'Content-Type': 'application/json',
        },
      }).then((response) => {
        if (response.data.status) {
          resolve(response.data.data);
        } else {
          reject(response);
        }
      }).catch((response) => {
        reject(response);
      })
    });
  },

  getListCategoryWithAuth: (data) => {
    const url = `${process.env.serverApi}/v1.0/category_with_auth`;
    return new Promise((resolve, reject) => {
      axios.get(url, {params: data, withCredentials: true, xsrfCookieName: false,}, {
        headers: {
          'Content-Type': 'application/json',
        },
      }).then((response) => {
        if (response.data.status) {
          resolve(response.data.data);
        } else {
          reject(response);
        }
      }).catch((response) => {
        reject(response);
      })
    });
  },

  deleteCate: (data) => {
    const url = `${process.env.serverApi}/v1.0/category/` + data.cateId;
    return new Promise((resolve, reject) => {
      axios.delete(url, data)
        .then((response) => {
          if (response.data.status) {
            resolve(response.data);
          } else {
            reject(response.data);
          }
        }).catch((response) => {
        reject(response);
      })
    });
  },

  createCate: (data) => {
    const url = `${process.env.serverApi}/v1.0/category`;
    return new Promise((resolve, reject) => {
      axios.post(url, data)
        .then((response) => {
          if (response.data.status) {
            resolve(response.data);
          } else {
            reject(response.data);
          }
        }).catch((response) => {
        reject(response);
      })
    });
  },

  updateCategory: (data) => {
    const url = `${process.env.serverApi}/v1.0/category/` + data._id;
    return new Promise((resolve, reject) => {
      axios.put(url, data)
        .then((response) => {
          if (response.data.status) {
            resolve(response.data);
          } else {
            reject(response.data);
          }
        }).catch((response) => {
        reject(response);
      })
    });
  },
}
