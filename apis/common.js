import axios from "axios";

export default {
  uploadImage: (data) => {
    const url = `${process.env.serverApi}/v1.0/upload`;
    return new Promise((resolve, reject) => {
      axios.post(url, data, {
        'Content-Type': 'multipart/form-data;'
      })
        .then((response) => {
          if (response.data.status) {
            resolve({
              url: `${process.env.serverApi}/public/` + response.data.data
            });
          } else {
            reject(response.data.message);
          }
        }).catch((response) => {
        reject('Upload failed');
      })
    });
  },
}
