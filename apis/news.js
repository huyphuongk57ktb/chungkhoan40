import axios from "axios";

export default {
  getListPost: (data) => {
    const url = `${process.env.serverApi}/v1.0/post`;
    return new Promise((resolve, reject) => {
      axios.get(url, {params: data, withCredentials: true, xsrfCookieName: false,}, {
        headers: {
          'Content-Type': 'application/json',
        },
      }).then((response) => {
        if (response.data.status) {
          resolve(response.data.data);
        } else {
          reject(response);
        }
      }).catch((response) => {
        reject(response);
      })
    });
  },

  getListPostWithAuth: (data) => {
    const url = `${process.env.serverApi}/v1.0/post_with_auth`;
    return new Promise((resolve, reject) => {
      axios.get(url, {params: data, withCredentials: true, xsrfCookieName: false,}, {
        headers: {
          'Content-Type': 'application/json',
        },
      }).then((response) => {
        if (response.data.status) {
          resolve(response.data.data);
        } else {
          reject(response);
        }
      }).catch((response) => {
        reject(response);
      })
    });
  },

  detailPost: (data) => {
    const url = `${process.env.serverApi}/v1.0/post/detail`;
    return new Promise((resolve, reject) => {
      axios.get(url, {params: data, withCredentials: true, xsrfCookieName: false,}, {
        headers: {
          'Content-Type': 'application/json',
        },
      }).then((response) => {
        if (response.data.status) {
          resolve(response.data.data);
        } else {
          reject(response);
        }
      }).catch((response) => {
        reject(response);
      })
    });
  },

  createPost: (data) => {
    const url = `${process.env.serverApi}/v1.0/post`;
    return new Promise((resolve, reject) => {
      axios.post(url, data)
        .then((response) => {
          if (response.data.status) {
            resolve(response.data);
          } else {
            reject(response.data);
          }
        }).catch((response) => {
        reject(response);
      })
    });
  },

  updatePost: (data) => {
    const url = `${process.env.serverApi}/v1.0/post/${data.id_post}`;
    return new Promise((resolve, reject) => {
      axios.put(url, data)
        .then((response) => {
          if (response.data.status) {
            resolve(response.data);
          } else {
            reject(response.data);
          }
        }).catch((response) => {
        reject(response);
      })
    });
  },

  deletePost: (data) => {
    const url = `${process.env.serverApi}/v1.0/post/` + data.postId;
    return new Promise((resolve, reject) => {
      axios.delete(url, data)
          .then((response) => {
            if (response.data.status) {
              resolve(response.data);
            } else {
              reject(response.data);
            }
          }).catch((response) => {
        reject(response);
      })
    });
  },
}
