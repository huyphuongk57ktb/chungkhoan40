import axios from "axios";

export default {
  listCommand: (data) => {
    const url = `${process.env.serverApi}/v1.0/message`;
    return new Promise((resolve, reject) => {
      axios.get(url, {params: data, withCredentials: true, xsrfCookieName: false,}, {
        headers: {
          'Content-Type': 'application/json',
        },
      }).then((response) => {
        if (response.data.status) {
          resolve(response.data.data);
        } else {
          reject(response);
        }
      }).catch((response) => {
        reject(response);
      })
    });
  },

  sendToken: (data) => {
    const url = `${process.env.serverApi}/v1.0/device/register`;
    return new Promise((resolve, reject) => {
      axios.post(url, data, {
        headers: {
          'Content-Type': 'application/json',
        },
        withCredentials: true,
        xsrfCookieName: false,
      }).then((response) => {
        if (response.status) {
          resolve(response);
        } else {
          reject(response);
        }
      }).catch((response) => {
        reject(response);
      })
    });
  },
}
