import axios from 'axios';

export default {
  authUser: (data) => {
    const url = `${process.env.serverApi}/v1.0/authen/login`;
    return new Promise((resolve, reject) => {
      axios.post(url, data)
        .then((response) => {
          if (response.data.status) {
            resolve(response.data);
          } else {
            reject(response.data);
          }
        }).catch((response) => {
        reject(response);
      })
    });
  },

  // logoutUser: (data) => {
  //   const url = `${process.env.api}/action-logout`;
  //   return new Promise((resolve, reject) => {
  //     axios.post(url, data)
  //       .then((response) => {
  //         if (response.data.status) {
  //           resolve(response.data);
  //         } else {
  //           reject(response.data);
  //         }
  //       }).catch((response) => {
  //       reject(response);
  //     })
  //   });
  // }
}
