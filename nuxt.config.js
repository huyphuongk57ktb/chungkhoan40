import fs from "fs";

export default {
  ssr: false,
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'chungkhoan40',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      // { name: 'viewport', content: 'width=device-width, initial-scale=1.0,shrink-to-fit=no' },
      { hid: 'description', name: 'description', content: 'Chứng khoán' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,400;0,500;0,700;1,500;1,700&display=swap'
      }
    ]
  },

  env: {
    serverApi: process.env.SERVER_API,
    clientUrl: process.env.CLIENT_URL
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    'element-ui/lib/theme-chalk/index.css',
    '~/assets/less/client/home.less',
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '@/plugins/element-ui',
    '@/plugins/core-components.js',
    '@/plugins/bus',
    '@/filters/index',
    { src: '~/plugins/infiniteloading', ssr: false }
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    '@nuxtjs/moment',
    '@nuxtjs/firebase',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [],

  firebase: {
    lazy: false,
    config: {
      apiKey: "AIzaSyDXvZ7ZR-sMThelyZ5kJ-t__vyzkM9ohHI",
      authDomain: "stock-9ed2c.firebaseapp.com",
      databaseURL: "https://stock-9ed2c-default-rtdb.asia-southeast1.firebasedatabase.app",
      projectId: "stock-9ed2c",
      storageBucket: "stock-9ed2c.appspot.com",
      messagingSenderId: "703915758347",
      appId: "1:703915758347:web:7cedc124cc175c5107880e",
      measurementId: "G-7FWSR2KPTN"
    },
    onFirebaseHosting: false,
    terminateDatabasesAfterGenerate: true,
    services: {
      auth: true,
      performance: true,
      analytics: true,
      // breaks the app with 'app.$fire.firestore.collection is not a function':
      appCheck: true,
      messaging: {
        createServiceWorker: false,
        fcmPublicVapidKey: "BOMgjQB2I6vPfuYq6aCb15zahcwFcvdk2X8T6c3uADUJ-wRYgGjWXqh2r3T0Lb7TBKdg2tFznLXjq6a7gkrNTmA",
        inject: fs.readFileSync('./static/firebase-messaging-sw.js', 'utf8'),
      },
      firestore: true
    },
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    transpile: [/^element-ui/],
    postcss: {
      preset: {
        features: {
          customProperties: false
        }
      }
    },
  },
  // server: {
  //   https: {
  //     key: fs.readFileSync(path.resolve(__dirname, 'server.key')),
  //     cert: fs.readFileSync(path.resolve(__dirname, 'server.crt'))
  //   }
  // },
  target: 'static',
}
