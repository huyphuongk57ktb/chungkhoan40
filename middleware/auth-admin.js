import axios from 'axios';
import {getters} from "../store/auth";

export default function({context, route, store, redirect}) {
  if (!store.getters['auth/isAuthenticated']) {
    redirect('/admin/login');
  }

  // Add a request interceptor
  axios.interceptors.request.use((config) => {
    // Do something before request is sent
    config.headers['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');
    // config.headers['Content-Type'] = 'application/json';
    config.withCredentials = true;
    // config.xsrfCookieName = '';
    // config.timeout = 10000;

    return config;
  }, function (error) {
    // Do something with request error
    console.log("lỗi req");
    return Promise.reject(error);
  });

  // Add a response interceptor
  axios.interceptors.response.use(function (response) {
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    return response;
  }, function (error) {
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    if (401 === error.response.status || 403 === error.response.status) {
      redirect('/admin/login');
    }
    return Promise.reject(error);
  });
}
