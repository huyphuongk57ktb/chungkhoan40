export const state = () => ({
  postView: {
    postId: '',
    title: '',
    categoryID: '',
    description: ''
  },
  postDetail: {
    title: '',
    description: '',
    categoryID: '',
    hasImage: true,
    images: [],
    content: ""
  },
});

export const getters = {
  getPostView: state => state.postView,
  getPostDetail: state => state.postDetail,
}

export const mutations = {
  changePostView(state, info) {
    state.postView = info;
  },

  changePostDetail(state, info) {
    console.log('2312312', info)
    state.postDetail = info;
  },
}

export const actions = {
  updateStatePostView({commit}, payload) {
    commit('changePostView', payload);
  },

  updateStatePostDetail({commit}, payload) {
    commit('changePostDetail', payload);
  }
}
