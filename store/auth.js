import api from '../apis/auth';

export const state = () => ({
  isCollapse: false,
  token: process.client ? localStorage.getItem('access_token') || '' : '',
  userInfo: {
    name: ''
  }
})

export const getters = {
  isCollapse: state => state.isCollapse,
  isAuthenticated: state => state.token,
  userInfo: state => state.userInfo,
}

export const mutations = {
  changeIsCollapse(state) {
    state.isCollapse = !state.isCollapse;
  },
  updateUserInfo(state, info) {
    state.userInfo = info;
  },
  setToken(state, token) {
    state.token = token;
  },
}

export const actions = {
  async AUTH_REQUEST({commit, dispatch}, data = {}) {
    try {
      const userInfo = await api.authUser(data);

      if (userInfo.status) {
        localStorage.setItem('access_token', userInfo.data.token);
        commit('setToken', userInfo.data.token);
        console.log('userInfo', userInfo);

        return userInfo;
        // commit('updateUserInfo', userInfo.data.user);
        // location.href = 'https://dev-vvi.vn:3000/projects'
      }
    } catch (e) {
      console.log('Đăng nhập lỗi!!!', e);
      return e;
    }
  },

  async AUTH_LOGOUT({commit, dispatch}, data = {}) {
    try {
      localStorage.removeItem('access_token');
      commit('setToken', '');
      location.reload();
    } catch (e) {
      console.log('Đăng xuất lỗi!!!', e);
    }
  }
}
