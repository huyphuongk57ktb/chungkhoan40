import Vue from 'vue';
import {formatDate} from './format-date.js';

Vue.filter('formatDate', formatDate);
