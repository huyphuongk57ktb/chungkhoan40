import moment from "moment";

const formatDate = (date, format = 'DD-MM-YYYY') => {
  if (date == null) {
    return '-';
  }
  return moment(date).format(format);
}

export {
  formatDate
}
