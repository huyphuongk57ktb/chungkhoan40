"use strict";

var fs = require('fs');
var path = require('path');
var http = require('http');
const express = require('express');
const app = express();

var staticBasePath = './dist';
var port = 3000;

app.set('port', port);
app.use(express.static(path.resolve(staticBasePath)));
app.get('*', function(req, res) {
  res.sendFile(path.resolve(staticBasePath) + '/index.html');
});
http.createServer(app).listen(port, () => {
  console.log('Http server running at ' + port);
});
