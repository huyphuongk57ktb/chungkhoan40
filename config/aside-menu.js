import fa from "element-ui/src/locale/lang/fa";

const asideMenu = [
  {
    'key': '1',
    'role': 1,
    'name': 'Danh Mục',
    'icon': 'el-icon-folder',
    'to': '/admin/categories',
    'child': false,
    'children': [
      {
        'key': '11',
        'name': 'Tạo mới khách hàng',
        'to': '/customers',
      },
      {
        'key': '12',
        'name': 'Danh sách đơn hàng',
        'to': '/order'
      },
    ]
  },
  {
    'key': '2',
    'role': 3,
    'name': 'Bài Viết',
    'icon': 'el-icon-postcard',
    'to': '/admin/posts',
    'child': false,
    'children': []
  }
]

export default asideMenu
