const editorConfig = {
  extraAllowedContent: 'h3{clear};h2{line-height};h2 h3{margin-left,margin-top}',
  //
  // // Adding drag and drop image upload.
  extraPlugins: 'print,format,font,colorbutton,justify',
  // removePlugins: 'image,pastefromword,pastefromgdocs',
  filebrowserUploadUrl: `${process.env.banhangDomain}/upload/ckeditor?`,
  filebrowserImageUploadUrl: `${process.env.banhangDomain}/upload/ckeditor?`,
  height: 200,
  language: 'vi',
  toolbar: [
    // { name: 'document', items: ['Print'] },
    { name: 'clipboard',   items: [ 'Cut', 'Copy',  'Paste', 'PasteText', 'Undo', 'Redo' ] },
    // { name: 'clipboard', items: ['Undo', 'Redo'] },
    { name: 'styles', items: ['Format', 'Font', 'FontSize'] },
    { name: 'colors', items: ['TextColor', 'BGColor'] },
    { name: 'align', items: ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'] },
    '/',
    {
      name: 'basicstyles',
      items: ['Bold', 'Italic', 'Underline', 'Strike', 'RemoveFormat', 'CopyFormatting']
    },
    { name: 'links', items: ['Link', 'Unlink'] },
    {
      name: 'paragraph',
      items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote']
    },
    { name: 'insert', items: ['Image', 'Table', 'base64image'] },
    { name: 'tools', items: ['Maximize'] },
    { name: 'editing', items: ['Scayt'] }
  ]
};
export default editorConfig;
